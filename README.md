### Koa bootstrap application

##### Includes :
- [Koa](https://koajs.com/) **Core framework**
- [Cors](https://github.com/koajs/cors)
- [Koa-router](https://github.com/ZijianHe/koa-router)
- [ESlint](https://eslint.org/) **Airbnb rules**
- [Sequelize](https://sequelize.org/) **ORM**
- [Koa-body](https://github.com/dlau/koa-body) **URL Parser**
- [Koa-json](https://github.com/koajs/json) **Send JSON**
- [Koa-logger](https://github.com/koajs/logger)
- [Bcrypt](https://github.com/kelektiv/node.bcrypt.js/)
- [Joi](https://github.com/hapijs/joi) **Validator**
- [JWT](https://github.com/koajs/jwt)
- [Moment](https://momentjs.com/)

TEST
- [Jest](https://jestjs.io/en/)
- [Supertest](https://github.com/visionmedia/supertest)


###### Change dev configuration for Sequelize in

> config/database.json

#### Run Application
---

`npm i`

`npm run dev`

### Tests
---

###### Change test configuration for Sequelize in

> config/database.json

`npm i`

`npm run test`

