const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    pseudo: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});
  User.associate = (models) => {
    // associations can be defined here
  };

  User.generateJWT = (user) => {
    const payload = { id: user.id, email: user.email, pseudo: user.pseudo };
    return {
      access_token: jwt.sign(payload, config.JWT_SECRET, { expiresIn: '100d' }),
      expires_at: 8640000,
    };
  };

  return User;
};
