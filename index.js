const Koa = require('koa');
const { middlewares } = require('./src/utils/middlewares');
const router = require('./src/routes');

const PORT = process.env.PORT || 3000;

const app = new Koa();

middlewares(app);

app.use(router.routes()).use(router.allowedMethods());

if (!module.parent) { // * Enable testing
  app.listen(PORT);
}

module.exports = app;
