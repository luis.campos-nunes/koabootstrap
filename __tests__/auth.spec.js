/* global test, expect, describe */
const supertest = require('supertest');
const app = require('../index');


describe('Auth : Routes', () => {
  const user = {
    pseudo: 'demo',
    email: 'demo@demo.com',
    password: 'demo',
    password_confirmation: 'demo',
  };

  test('POST /auth/register', async () => {
    const response = await supertest(app.callback())
      .post('/auth/register')
      .send(user)
      .set('Accept', 'application/json');

    expect(response).toBeDefined();
    expect(response.statusCode).toBe(200);
    expect(response.body.access_token).toMatch(new RegExp('[a-zA-Z0-9-_]+?.[a-zA-Z0-9-_]+?.([a-zA-Z0-9-_]+)[/a-zA-Z0-9-_]+?$'));
    expect(response.body.expires_at).toBe(8640000);
  });
  test('POST /auth/login', async () => {
    const { email, password } = user;

    const response = await supertest(app.callback())
      .post('/auth/login')
      .send({ email, password })
      .set('Accept', 'application/json');

    expect(response.statusCode).toBe(200);
    expect(response.body.access_token).toMatch(new RegExp('[a-zA-Z0-9-_]+?.[a-zA-Z0-9-_]+?.([a-zA-Z0-9-_]+)[/a-zA-Z0-9-_]+?$'));
    expect(response.body.expires_at).toBe(8640000);
  });
});
