const Router = require('koa-router');
const Auth = require('./auth');
const Dogs = require('./dogs');
const { crypt } = require('../utils/password');

const router = new Router();

router.get('/', async (ctx) => {
  const crypted = await crypt('demo');
  ctx.type = 'application/json';
  ctx.status = 200;
  ctx.body = { cryptedpass: crypted };
});
router.use(Auth.routes(), Auth.allowedMethods());
router.use(Dogs.routes(), Dogs.allowedMethods());

module.exports = router;
