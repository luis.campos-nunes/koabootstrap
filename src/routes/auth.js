const Router = require('koa-router');

const AuthController = require('../controllers/auth');
const AuthValidator = require('../validators/auth');

const router = new Router({
  prefix: '/auth',
});

router.post('/login', AuthValidator.login, AuthController.login);

router.post('/register', AuthValidator.register, AuthController.register);

module.exports = router;
