const { User } = require('../../models');
const { crypt, compare } = require('../utils/password');
const ServerError = require('../utils/serverError');


module.exports = class AuthManager {
  constructor(data) {
    this.data = data;
  }

  async login() {
    const user = await User.findOne({ where: { email: this.data.email } });

    if (!user) {
      throw new ServerError(422, 'L\'Email est incorrect.');
    }

    const match = await compare(this.data.password, user.password);

    if (!match) {
      throw new ServerError(422, 'Le mot de passe est incorrect.');
    }

    return User.generateJWT(user);
  }

  async register() {
    const userPseudo = await User.count({ where: { pseudo: this.data.pseudo } });
    const userEmail = await User.count({ where: { email: this.data.email } });

    if (userPseudo && userEmail) {
      throw new ServerError(422, 'Le pseudo et l\'adresse mail sont déjà utilisé.');
    } else if (userPseudo && !userEmail) {
      throw new ServerError(422, 'Le pseudo existe déjà.');
    } else if (userEmail && !userPseudo) {
      throw new ServerError(422, 'Cet email est déjà utilisé');
    }

    this.data.password = await crypt(this.data.password);

    const user = await User.create(this.data);

    return User.generateJWT(user);
  }
};
