const ServerError = require('../utils/serverError');

module.exports = {
  async validate(schema, ctx, next) {
    try {
      await schema.validateAsync(ctx.request.body);
      await next();
    } catch (error) {
      throw new ServerError(422, error.message);
    }
  },
};
