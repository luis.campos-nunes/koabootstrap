const Joi = require('@hapi/joi');
const { validate } = require('./index');

const loginSchema = {
  email: Joi.string().email().required(),
  password: Joi.string().required(),
};

const registerSchema = {
  ...loginSchema,
  pseudo: Joi.string().required(),
  password_confirmation: Joi.required().valid(Joi.ref('password')),
};

module.exports = {
  async login(ctx, next) {
    await validate(Joi.object(loginSchema), ctx, next);
  },

  async register(ctx, next) {
    await validate(Joi.object(registerSchema), ctx, next);
  },
};
