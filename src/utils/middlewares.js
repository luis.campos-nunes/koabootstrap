const logger = require('koa-logger');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const json = require('koa-json');
const jwt = require('../../middlewares/jwt');
const error = require('../../middlewares/error');

module.exports = {
  middlewares(app) {
    app.use(error);
    app.use(logger());
    app.use(cors());
    app.use(bodyParser());
    app.use(json());
    app.use(jwt);
  },
};
