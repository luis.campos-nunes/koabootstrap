const bcrypt = require('bcrypt');

module.exports = {
  async crypt(password) {
    const saltRounds = 10;
    const hashed = await new Promise((resolve, reject) => {
      bcrypt.hash(password, saltRounds, (err, hash) => {
        if (err) reject(err);
        resolve(hash);
      });
    });
    return hashed;
  },

  async compare(password, hash) {
    const match = await bcrypt.compare(password, hash);
    return match;
  },
};
