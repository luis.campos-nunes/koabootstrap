const AuthManager = require('../managers/auth');

module.exports = {
  async login(ctx) {
    const authManager = new AuthManager(ctx.request.body);
    const access = await authManager.login();
    ctx.status = 200;
    ctx.body = access;
  },

  async register(ctx) {
    const authManager = new AuthManager(ctx.request.body);
    const access = await authManager.register();
    ctx.status = 200;
    ctx.body = access;
  },
};
