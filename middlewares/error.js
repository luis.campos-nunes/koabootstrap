module.exports = async (ctx, next) => next().catch((error) => {
  const { status, message } = error;
  ctx.type = 'json';
  ctx.status = status || 500;
  ctx.body = {
    status: 'error',
    message,
  };
  ctx.app.emit('error', error, ctx);
});
