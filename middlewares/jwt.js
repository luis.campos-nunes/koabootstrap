const JWT = require('jsonwebtoken');
const config = require('../config/config.json');
const ServerError = require('../src/utils/serverError');

const jwt = async function jwt(ctx, next) {
  const paths = config.AUTHORIZED_ROUTES;
  if (paths.includes(ctx.request.path)) {
    return next();
  }
  
  if (!ctx.request.headers.authorization) {
    throw new ServerError(401, 'JWT is required.');
  }

  const token = ctx.request.headers.authorization.split(' ')[1];

  try {
    const decoded = await JWT.verify(token, config.JWT_SECRET);
    ctx.user = decoded;
    return next();
  } catch (error) {
    throw new ServerError(401, 'JWT is required.');
  }
};

module.exports = jwt;
